package com.servletdemo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SourceServlet
 */
@WebServlet("/SourceServlet")
public class SourceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("name");
		String pass1 = request.getParameter("pwd1");
		String pass2 = request.getParameter("pwd2");
		response.setContentType("text/html");
		RequestDispatcher rd = request.getRequestDispatcher("TargetServlet");
		List<String> course = new ArrayList<String>();
		PrintWriter pw = response.getWriter();
		course.add("Core Java");
		course.add("J2EE");
		course.add("J2ME");
		course.add("HTML,CSS,Javascript");
		request.setAttribute("courses",course);
		if(pass1.equals(pass2)) {
			pw.println("Hello "+name);
			//rd.forward(request, response);
			rd.include(request, response);
		}else {
			response.sendRedirect("Error.html");
		}
		
	}

}
