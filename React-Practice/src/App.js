import React, { Component } from 'react';
import './App.css';
// import Clock from './components/clock';
// import Register from './components/register'
import Routes from './routes';
import Users from './components/FetchAllUsers';
import FetchPost from './components/FetchPost'

class App extends Component {
  render() {
    return (
     
      <div>
        {/* <Routes /> */}
        <Users/>
        <FetchPost />
      </div>
      
    
    );
  }
}

export default App;
