import React, {Component} from  'react';

class Clock extends Component {

    constructor (props){
        super(props);
        this.state = {
            date : new Date()
        }
    }
    componentDidMount() {
        console.log ("componentDidMount is called");
        this.timerID = setInterval(
            () => this.tick(),
            1000
            );
        }
        tick() {
            this.setState({
              date: new Date()
            });
          }
    render() { 
        console.log ("render is called");
        return (  

           <div>
               <h1>Clock</h1>
               <h2>{this.state.date.toLocaleTimeString()}</h2>
            </div> 
        );
    }
}
 
export default Clock;