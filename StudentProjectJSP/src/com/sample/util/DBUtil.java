package com.sample.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * The DButil class is used to connect Database to the java program.
 * @author IMVIZAG
 *
 */
public class DBUtil {
	/**
	 * this method establish the connection from Database.
	 * @return
	 */
	public static Connection getCon() {
		Connection con = null;
		
		try {
			//1.loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			//2.establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/student", "root", "innominds");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	
	
}
