package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Student;
import com.sample.util.DBUtil;
/**
 *  The StudentDAOImpl class is used to implemnt all abstract methods in  StudentDAO interface.
 * @author IMVIZAG
 *
 */

public class StudentDAOImpl implements StudentDAO {
	/**
	 * This method calls to insert a new student details
	 *
	 */
	@Override
	public boolean createStudent(Student student) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_info values( ?, ?, ?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			
			
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	/**
	 * This method calls to SEARCH  a  student details by student id..
	 */
	@Override
	public Student searchById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_info where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}
	/**
	 * This method calls to Fetch All student details
	 */
	@Override
	public List<Student> getAllStudents() {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List <Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_info ";
		try {
			con = DBUtil.getCon();
			stmt= con.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				st= new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return studentList;
	}
	/**
	 * This method calls to update  a  student details.
	 */
	public boolean UpdateById(int id , int age, String name) {
		boolean result = false;
		Connection con = null;
		PreparedStatement ps = null;
		String sql = "update student_info set name = ? , age = ?  where id = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setInt(3, id);
			int i = ps.executeUpdate();
			if(i == 1) {
				result = true;
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	/**
	 * This method is used to delete an employee by id.
	 */
	public boolean DeleteById(int id) {
		boolean result = false;
		Connection con = null;
		PreparedStatement ps = null;
		String sql = "delete from student_info where id = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int i = ps.executeUpdate();
			if(i == 1) {
				result = true;
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

}











