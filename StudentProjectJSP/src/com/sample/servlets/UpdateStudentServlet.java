package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudentServlet")
public class UpdateStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		if (session != null) {
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("uname");
		int age = Integer.parseInt(request.getParameter("uage"));
		if(ckeckStudent(id)) {
			boolean result = new StudentServiceImpl().UpdateById(id,age,name);
			
			if(result) {
				RequestDispatcher rd = request.getRequestDispatcher("Update.jsp");
				rd.forward(request, response);
			}
			else {
				response.sendRedirect("error.html");
			}
			}
		}else {
				response.sendRedirect("Unauthorized.html");
			}
		
		
	}
	public boolean ckeckStudent(int id) {
		boolean result = false;
		StudentService std = new StudentServiceImpl();
		Student student = std.findById(id);
		if (student != null) {
			result = true;
		} else {
			result = false;
		}
		return result;

	}
}
