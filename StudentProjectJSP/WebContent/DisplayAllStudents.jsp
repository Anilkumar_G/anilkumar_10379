<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Student Data</title>
<style>
.box{
  background:#f0776c;
  width:300px;
  border-radius:8px;
  margin: 100px auto 0 auto;
  padding:0px 0px 70px 0px;
  border: white 4px solid;
  align-items: center;
  }

.result {
    border: solid 1px black;
    border-collapse: collapse;
    border-spacing: 0;
    font: normal 13px Arial, sans-serif;
    align-content: center;
}
.result thead th {
    background-color: #f0776c;
    border: solid 1px black;
    color: black;
    padding: 10px;
    text-align: left;
    text-shadow: 1px 1px 1px #fff;
}
.result tbody td {
    border: solid 1px #DDEEEE;
    color: #333;
    padding: 10px;
    text-shadow: 1px 1px 1px #fff;
}
</style>
</head>
<body style = "background-color:#2d343d">
<div class="box">
<table class="result">
	<thead>
		<tr>
			<th>Student Id</th>
			<th>Student Nam</th>
			<th>Student Age</th>
		</tr>
	</thead>
<c:forEach var ="student" items="${students}">
	
		
		<tr>
			<td>${student.id }</td>
			<td>${student.name }</td>
			<td>${student.age }</td>
		<tr>

</c:forEach>
</table>
</div>
</body>
</html>