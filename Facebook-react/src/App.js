import React, { Component } from 'react';
import Navbar from './components/Header/navbar';
import Leftbar from './components/leftbar/leftbar';
import Rightbar from './components/rightbar/ActiveFriends';
import Newsfeed from './components/newsfeed/newsfeed';
import Stories from './components/stories/stories';

class App extends Component {
  render() {
    let style1 = {
      position:"fixed"
    }
    let style2 = {
      overflow: "auto"
    }
    return (
      <div>
        <Navbar style={style1}/>
        <div class="container-fluid">
          <div class="row">
            <div class = "col-lg-2">
              <Leftbar style={style1}/>
            </div> 
            <div class = "col-lg-6">
            <Newsfeed />
            </div> 
            <div class = "col-lg-2">
            <Stories />
            </div>
            <div class = "col-lg-2">
              <Rightbar style={style1}/>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

export default App;
