import React, {Component} from 'react';

class stories extends Component {
    render() { 
        let styleA = {
            marginTop :"16px"
        } 
        let style1 = {
            backgroundColor : "white",
            marginTop : "16px",
            borderRadius : "5px"
        }
        let style2 = {
            fontSize : "16px",
            marginTop : "10px",
            marginLeft : "10px"
        }
        let style6 = {
            marginTop : "20px"
        }
        let style7 = {
            fontSize : "18px",
            color : "red"
        }
        let style9 = {
            fontSize : "18px",
            color : "purple"
        }
        let style10 = {
            opacity : "0.5"
        }
        return ( 
            <div class="container-fluid" style={styleA}>
                <div style= {style1}>
                    <h4> stories</h4>
                    <div>
                     <p><span><i class="far fa-user-circle" style={style2}></i></span>Add to your story</p>
                     <p><span><i class="far fa-user-circle" style={style2}></i></span>see Story</p>
                     <p><span><i class="far fa-user-circle" style={style2}></i></span>see Story</p>
                     <p><span><i class="far fa-user-circle" style={style2}></i></span>see Story</p>
                     <p><span><i class="far fa-user-circle" style={style2}></i></span>see Story</p>
                    </div>
                    </div>
                    <div class="container-fluid" style={styleA}>
                    <div style= {style1}>
                    <div>
                    <p style= {style6}><i class="fas fa-bookmark" style= {style9}></i>&nbsp;&nbsp;Saved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={style10}>3</span></p>
                    <p style= {style6}><i class="fas fa-calendar-week" style= {style7}></i>&nbsp;&nbsp;Events&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={style10}>1</span></p>
                     </div>      
                    </div>
                </div>
            </div>
         );
    }
}
 
export default stories;
