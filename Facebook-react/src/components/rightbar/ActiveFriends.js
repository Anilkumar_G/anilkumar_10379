import React, {Component} from 'react';

class rightBar extends Component {
    render() { 
        let style1 = {
            marginTop : "10px"
        }
        let style2 ={
            color: "green",
            fontSize : "8px",
        }
        return (  
            <div>
                <div class="container-fluid">
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Anilkumar G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Anusha Netha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Avinash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Brahmaiah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Chitra&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Harish Routhu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Khaza Bro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Raghu Rags&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Awesome Gopi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Sowjanya Sowju&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p>
                            <p style= {style1}><span><i class="fas fa-user"></i></span>&nbsp;&nbsp;&nbsp;&nbsp;Sreekanth   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><i class="fas fa-circle" style = {style2}></i></span></p> 
                            </div>
                        </div>
        );
    }
}
 
export default rightBar;
