import React, { Component } from 'react';
import './table.css';
import {Link} from 'react-router-dom';

class DisplayAllEmployees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/manager/displayEmployee')
            .then(response => response.json())
            .then(employeeData => {
                this.setState({
                    isLoaded: true,
                    employees: employeeData
                })
                
            });
            
    }
    render() {
        
        var { isLoaded, employees } = this.state;
        
        if (!isLoaded) {
            return <div><h1>Loading...</h1></div>;
        }
        console.log (employees);
        return (
        <div>
            <table id = "display">
            <thead>Employee Details</thead>
                <tr><th>Employee Id</th><th>Name</th><th>Role</th></tr>
                {employees.map(employee => (
                    <tr key ={employees.employeeId}>
                        <td>{employee.employeeId}</td>
                        <td>{employee.employeeName}</td>
                        <td>{employee.role}</td>
                    </tr>
                ))}
            </table>
            <hr/>
            <Link to = "/">Go Back</Link>
        </div>
        );
    }
}
export default DisplayAllEmployees;