import React, { Component } from 'react';
import './form.css';

class AddProducts extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            productname : '',
            price : '',
            quantity :'',
            productType : ''
         }
    }
    handleChange = event => {
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = event =>{
        event.preventDefault();
        console.log("name: "+this.state.productname);
        console.log("price: "+this.state.price);
        console.log("quantity: "+this.state.quantity);
        console.log("producttype: "+this.state.productType);
        const data = {
            productname:this.state.productname,
            price:this.state.price,
            quantity:this.state.quantity,
            productType:this.state.productType
        }
        fetch("http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts",{
            method : 'POST',
            headers :{'content-Type' : 'application/json'},
            body : JSON.stringify(data)
        })
        .then((res) => res.json)
        .then((data) => alert("Producte Added Successfully"))
        .catch((err) => console.log(err))
    }
    render() { 
        return ( 
            <div>  
                <form onSubmit={this.handleSubmit}>
                    Enter Product Name: <br/>
                    <input type="text" name="productname" value ={this.state.productname} placeholder="Enter Product Name" onChange={this.handleChange}/><br/>
                    Enter Product Price: <br/>
                    <input type="text" name ="price" value ={this.state.price} placeholder="Enter Product Price" onChange={this.handleChange}/><br/>
                    Enter Product Quantity: <br/>
                    <input type="text" name="quantity" value ={this.state.quantity} placeholder="Enter Product Quantity" onChange={this.handleChange}/><br/>
                    Product Type (Units or Weight) : <br/>
                    <input type="text" name="productType" value ={this.state.productType} placeholder="Enter Product Type" onChange={this.handleChange}/><br/>
                    <input type="submit" value= "Add Product"/>
                </form>
            </div>
         );
    }
}
 
export default AddProducts;

