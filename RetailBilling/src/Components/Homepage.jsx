import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './home.css'

class HomePage extends Component {
    render() { 
        return ( 
        <div>
            <div style= {{backgroundColor :"#4CAF50",marginTop : "-30px",textAlign : "center"}}>
            <h1 style= {{marginTop : "55px"}}>Welcome to Retail Billing System</h1>
           
            </div>
            <div>
            <ul>
            <li><Link to = "/employees">Employee Data</Link></li>
            <li><Link to = "/deleteEmployee" >Delete Employee</Link></li>
            <li><Link to = "/products">Show Products</Link></li>
            <li><Link to = "/addProduct">Add Products</Link></li>
            <li><Link to ="/increasequantity">Increase Qunatity</Link> </li>
            <li><Link to = "/deleteproduct" >Delete product</Link></li>
            </ul>
            </div>
        </div> 
        );
    }
}
 
export default HomePage;