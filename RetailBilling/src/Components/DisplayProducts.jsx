import React, { Component } from 'react';
import './table.css';
import {Link} from 'react-router-dom';

class DisplayAllProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts')
            .then(response => response.json())
            .then(productsData => {
                this.setState({
                    isLoaded: true,
                    products: productsData
                })
            });
    }
    render() {
        var { isLoaded, products } = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>;
        }
        return (
            <div>
                <h1 style ={{textAlign : "center"}}>All Product Details</h1>
                
            <table id = "display">
                <tr><th>Product Id</th><th>Product Name</th><th>Product Price</th><th>Product Quantity</th><th>Product productType</th></tr>
                {products.map(product => (
                    <tr key ={product.product_id}>
                        <td>{product.product_id}</td>
                        <td>{product.productname}</td>
                        <td>{product.price}</td>
                        <td>{product.quantity}</td>
                        <td>{product.productType}</td>
                    </tr>
                ))}
            </table>
            <Link to = "/">Go Back</Link>
        </div>
        );
    }
}
export default DisplayAllProducts;