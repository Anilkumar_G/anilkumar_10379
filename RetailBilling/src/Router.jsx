import React from 'react';
import Home from './Components/Homepage';
import {Switch,BrowserRouter,Route} from 'react-router-dom';
import DisplayAllEmployees from './Components/DisplayAllEmployee';
import DisplayAProducts from './Components/DisplayProducts';
import AddProduct from './Components/AddProducts';
import IncreaseQuntity from './Components/IncreaseQuantity';
import DeleteEmployee from './Components/DeleteEmployee';
import Deleteproduct from './Components/DeleteProduct';

const Routes = () => (
    <BrowserRouter>
    <Switch>
    <Route path = "/" component = {Home} exact/>
    <Route path = "/employees" component = {DisplayAllEmployees} exact />
    <Route path = "/products" component = {DisplayAProducts} exact />
    <Route path = "/addProduct" component = {AddProduct} exact />
    <Route path = "/increasequantity" component = {IncreaseQuntity} exact />
    <Route path = '/deleteEmployee' component = {DeleteEmployee} exact />
    <Route path = '/deleteProduct' component = {Deleteproduct} exact />
    </Switch>
    </BrowserRouter>
);

export default Routes;