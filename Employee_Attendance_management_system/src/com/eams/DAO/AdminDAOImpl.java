package com.eams.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.eams.bean.Admin;
import com.eams.util.DBConnectionUtil;
/**
 * This is implementation class for the AdminDAO interface.
 * @author Batch - G
 *
 */
public class AdminDAOImpl implements AdminDAO {
	/**
	 * this method is used to create an admin from the database.
	*/
	@SuppressWarnings("static-access")
	@Override
	public boolean createAdminDAO(Admin adminDetails) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into admin values( ?, ?)";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, adminDetails.getAdmin_username());
			ps.setString(2, adminDetails.getAdmin_password());
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;	
	}
	/**
	 * this method is used to create an admin from the database.
	 */
	@SuppressWarnings("static-access")
	@Override
	public  Admin searchByUsername(String username) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Admin adminDetails=null;
		String sql = "select * from admin where Admin_username = ? ";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			while(rs.next()) {
				adminDetails= new Admin();
				adminDetails.setAdmin_username(rs.getString(2));
				adminDetails.setAdmin_password(rs.getString(3));				
			}
		}
		catch (SQLException e){			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return adminDetails;
	}
	/**
	 * this method is used to delete an admin from the database.
	 * @return result
	 */
	
	@Override
	public boolean deleteAdminDAO(String username) {
		boolean result=false;
		Connection con = null;
		PreparedStatement ps = null;
		//ResultSet rs = null;
		//Admin adminDetails=null;
		String sql = "DELETE FROM admin WHERE Admin_username=?";
		try {
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			
		}
        return result;

	}
	/**
	 * This method is for adminlogin with his username and password
	 * 
	 */
	@Override
	public String AdminLogin(String username, String password ) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//String password = null;
		String sql = "select Admin_password from admin where Admin_username = ?";
		try {
			// getting connection object and executing query
			con = DBConnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);

			rs = ps.executeQuery();
			if (rs.next()) {
				password = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {

			}
		}

		return password;
	}

	}
	
	

	
	
