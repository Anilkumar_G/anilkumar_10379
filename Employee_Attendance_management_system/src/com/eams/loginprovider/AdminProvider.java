package com.eams.loginprovider;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import com.eams.DAO.AdminDAOImpl;
import com.eams.bean.Admin;
import com.google.gson.Gson;

/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author IMVIZAG
 *
 */
@Path("/adminprovider")
public class AdminProvider {
	
	/**
	 * This is resource method which performs findByUsername operation
	 * @param username
	 * @return user
	 */
	@POST
	@Path("/findByUsername")
	@Produces("application/json")
	public String findByUsername(@QueryParam("username") String username) {
		
		Gson gson = new Gson();
		Admin  list = new AdminDAOImpl().searchByUsername(username);
		String adminDetails = gson.toJson(list);
		return adminDetails;
		
	}
	/**
	 * This is resource method which performs deleteAdmin operation
	 * @param username
	 * @return deleteData
	 */
	@POST
	@Path("/deleteadmin")
	@Produces("application/json")
	 public String deleteAdmin(@QueryParam("username") String username) {
		 Gson gson = new Gson();
		 boolean delete =new AdminDAOImpl().deleteAdminDAO(username);
		 String deleteData =gson.toJson(delete);	 
		return deleteData;

	 }

}
