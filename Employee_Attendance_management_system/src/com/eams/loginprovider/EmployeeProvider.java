package com.eams.loginprovider;


import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import com.eams.DAO.EmployeeDAOImpl;
import com.eams.bean.Employee;
import com.google.gson.Gson;


/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author IMVIZAG
 *
 */
@Path("/employeelogin")
public class EmployeeProvider {
	

	/**
	 * This is resource method which performs fetchAllEmployee operation
	 * @param emp_id
	 * @return employeeList
	 */
	@POST
	@Path("/fetchAllEmployee")
	@Produces("application/json")
       public String fetchAllEmployee(@QueryParam("id") int emp_id) {
		Gson gson = new Gson();
		List <Employee> list = new EmployeeDAOImpl().fetchAllEmployeeDAO();
		System.out.println(list.size());
		String employeeList = gson.toJson(list);
		return employeeList;
   
       }
	/**
	 * This is resource method which performs findById operation
	 * @param emp_id
	 * @return empData
	 */
	@POST
	@Path("/findById")
	@Produces("application/json")
       public String findById(@QueryParam("id") int emp_id) {
		Gson gson = new Gson();
		List <Employee> list = new EmployeeDAOImpl().findByIdDAO(emp_id);
		System.out.println(list.size());
		String empData = gson.toJson(list);
		return empData;
   
       }
	/**
	 * This is resource method which performs updateEmployee operation
	 * @param phone_number
	 * @param emp_id
	 * @return updateData
	 */
	@POST
	@Path("/updateEmployee")
	@Produces("application/json")
       public String updateEmployee(@QueryParam("phone_number") String phone_number, @QueryParam("id") int emp_id) {
		Gson gson = new Gson();
		boolean update = new EmployeeDAOImpl().updateEmployeeDAO(emp_id, phone_number);
		String updateData = gson.toJson(update);
		return updateData;
	
	}
	/**
	 * This is resource method which performs deleteEmployee operation
	 * @param emp_id
	 * @return deleteData
	 */
	@POST
	@Path("/deleteEmployee")
	@Produces("application/json")
       public String deleteEmployee(@QueryParam("id") int emp_id) {
		Gson gson = new Gson();
		boolean delete = new EmployeeDAOImpl().deleteEmployeeDAO(emp_id);
		String deleteData = gson.toJson(delete);
		return deleteData;
	
	}
	/**
	 * This is resource method which performs insert new Employee operation
	 * @param Employee
	 * @return String
	 * 
	 */
	@SuppressWarnings("unused")
	@POST
	@Path("/insertEmployee")
	@Produces("application/json")
	@Consumes("application/json")
	public String insertEmployee(String employee) {
		Gson gson = new Gson();
		Employee employ = gson.fromJson(employee, Employee.class);
		boolean result = new EmployeeDAOImpl().insertEmployeeDAO(employ);		
		return "harish";			
    }
	
}