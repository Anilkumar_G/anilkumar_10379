package com.eams.loginprovider;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.eams.services.AdminServieImp;
import com.eams.services.EmployeeServiceImpl;


/**
 * This is the resource class which acts as web resource which is specified with
 * specific url
 * 
 * @author IMVIZAG
 *
 */
@Path("/login")
public class LoginProvider {
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/admin")
	@Produces("application/json")
	public String login(@QueryParam("username") String Admin_username, @QueryParam("password") String Admin_password) {
		// calling service method
		boolean isLogin = new AdminServieImp().AdminLogin(Admin_username, Admin_password);
		String res = isLogin ? Admin_username : "not user";
		return res;
	}
	@POST
	@Path("/employee")
	@Produces("application/json")
	public String EmployeeLoggin(@QueryParam("username") String emp_name, @QueryParam("password") String password) {
		 boolean isLogin = new EmployeeServiceImpl().employeeLogin(emp_name, password);
		
		String result = isLogin ? emp_name : "not an user";
		return result;
		
		


	
	
	
}
}
