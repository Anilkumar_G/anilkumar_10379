import React from 'react';

export default function SayHello(props) {
    return (
        <div>
        <h2>Say hello to {props.name}</h2>
        </div>
    )
}