import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import {Switch,Route} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Home from './components/Home';
import Cars from './components/cars';
import News from './components/news';
import CarDetails from './components/carDetails';
// import SinglePost from './components/SinglePost';
import newsDetails from './components/newsDetails';
import Login from './components/login';
import AdminDashBoard from './components/adminDashBoard';

const Routes = () => (
    <BrowserRouter>
    <Switch>
    <Route path = "/" component = {Home} exact/>
    <Route path = "/clock" component = {Clock} exact/>
    <Route path = "/register" component = {Register} exact/>
    <Route path = "/cars" component = {Cars} exact/>
    <Route path = {"/cars/:id"} component = {CarDetails} exact />
    <Route path = "/news" component = {News} exact />
    <Route path = {"/news/:id"} component = {newsDetails} exact />
    <Route path = "/login" component = {Login} exact />
    <Route path = "/admin/dashboard" component = {AdminDashBoard} exact />
    </Switch>
    </BrowserRouter>
);

export default Routes;