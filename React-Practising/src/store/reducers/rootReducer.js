import  { combineReducers } from 'redux';
import counterReducer from '../reducers/conterReducer';
import newsReducer from '../reducers/newsReducer'
import authReducer from "./authreducer";

const rootReducer = combineReducers ({
    counterReducer,
    newsReducer,
    authReducer
});

export default rootReducer;