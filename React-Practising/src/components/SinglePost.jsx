import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';
import './news.css';


class SinglePost extends Component {
    
    componentDidMount (){
        this.props.newsActions.fetchNews();
      
    }

    render() { 
        const id = this.props.match.params.id;  
        console.log(this.props.match.params.id);
        console.log (this.props.newsItems);
        // const post = this.props.newsItems.filter(news1 => {
        //     if (news1._id == id){
        //         return news1;
        //     }
        // });

        return ( 
            <div>
                <h1>i am here</h1>
                {this.props.newsItems.map(post => {
              if (post._id == id){
                  return <h1>{post.heading}</h1>
              }
          })};
            </div>
         );
    }
}

function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news,
    };

    }
    
function mapDispatchToProps(dispatch) {
        return {
            newsActions: bindActionCreators(newsActions, dispatch),
        };
        }
export default connect (mapStateToProps,mapDispatchToProps) (SinglePost);

