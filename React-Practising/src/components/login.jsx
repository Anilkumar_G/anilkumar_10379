import React, { Component } from 'react';
import './login.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import {Redirect} from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            isSubmitted : false
        }

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }

    doLogin = e => {
            this.props.authActions.doLogInUser({
                email : this.state.email,
                password : this.state.password
            });
            console.log(this.state.email)
            this.setState ({
                isSubmitted :true
            });
        }

    render() {

      if (this.state.isSubmitted){
          if (localStorage.getItem('jwt-token') && this.props.isLoggedIn === true){
              return <Redirect to ="/admin/dashboard" />
          }
          else {
              return (
                  <div>Login credentials Wrong</div>
              )
          }
      }
        
        return (
            <div className ="body">
            <div className = "background" style={{opacity : 0.5}}> </div>
            <div className="header">
                <div>Yo!<span>Aspire</span></div>
            </div><br/>    
                <div className = "login">
                {/* <form onSubmit = {this.doLogin} method = "post"> */}
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Enter email"></input><br/>
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder = "password"></input>
                        <br/>
                        <input type="submit" value="Login" onClick = {this.doLogin}   className ="login-button"></input>
                {/* </form> */}
                </div>
            </div>
           
        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn : state.authReducer.is_logged_in, 
    };

}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);