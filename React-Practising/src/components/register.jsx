import React, { Component } from 'react';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username : '',
            favourite : ''
         }
         this.handleChange = this.handleChange.bind(this);
         this.handleSubmit = this.handleSubmit.bind(this);
    }
        handleChange (event){
            this.setState ({
                [event.target.name] : event.target.value
            });
            
        }
        handleSubmit(event) {
            alert('A name was submitted: ' + this.state.username + 'fourites are  ' + this.state.favourite);
            event.preventDefault();
          }
    render() { 
        return ( 
            <div>
                <h3>Registration Form</h3>
                <form onSubmit = {this.handleSubmit}>
                <label>
                    Name:
                    <input type="text" name="username" value = {this.state.username} onChange = {this.handleChange}/><br/>
                </label>
                <label>
                    favourites:
                    <select name= "favourite" value = {this.state.favourite}  onChange = {this.handleChange}>
                        <option value="grapefruit" >Grapefruit</option>
                        <option value="lime">Lime</option>
                        <option selected value="coconut">Coconut</option>
                        <option value="mango">Mango</option>
                    </select><br/>
                </label>
                <input type="submit" value="Submit" />
                </form>
            </div>
         );
    }
}
 
export default Register;