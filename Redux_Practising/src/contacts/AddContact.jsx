import React, { Component } from 'react';
import * as contactActions from '../store/actions/contactActions';
import { connect } from 'react-redux'

class AddContact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            nameerror : ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateName = () => {
        const { name } = this.state;
        this.setState({
          nameError:
            name.length > 3 && name.length < 10 && name.include('@','.') ? null : 'Name must be in between 3 to 10',
          });
      }

    handleChange(e) {
        this.setState({
            name: e.target.value
        })

    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.name.length == 0){
            alert ("name should not be empty")
        }
        else{
        let contact = {
            name: this.state.name
        }
        this.props.createContact(contact);
        this.state.name = ''
    }
}

    listView(data, id) {
        return (
            <div className="row" >
                <div className="col-md-10">
                    <li key={id} className="list-group-item clearfix">
                        {data.name}
                    </li>
                </div>
                <div className="col-md-2">
                    <button onClick={(e) => this.deleteContact(e, id)} className="btn btn-danger">Remove</button>
                </div>
            </div>
        )
    }

    deleteContact(e, id) {
        console.log(id)
        e.preventDefault();
        this.props.deleteContact(id)
    }

    render() {
        return (
            <div className="container">
                <h1 style={{ overflow: "hidden" }}>Contacts Application</h1>
                <hr />
                <div className = "col-lg-10" >
                    <h3 style={{ overflow: "hidden" }}>Add Contact Form</h3>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" onChange={this.handleChange} className="form-control" value={this.state.name} /><br />
                        <input type="submit" className="btn btn-success" value="ADD" />
                    </form>
                    <hr />
                    {<ul className="list-group">
                        {this.props.contacts.map((contact, id) => this.listView(contact, id))}
                    </ul>}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createContact: contact => dispatch(contactActions.createContact(contact)),
        deleteContact: id => dispatch(contactActions.deleteContact(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddContact);