import * as allActions from '../actions/actionConstants';
import request from 'superagent';
import * as roleActions from '../actions/roleActions'

const roleService = store => next => action => {
    
    next(action)
    switch (action.type) {
        case allActions.FETCH_ROLE:
        
        request.get ("http://13.250.235.137:8050/api/user")
            .set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
            .then (res =>{
                const data = JSON.parse (res.text);
                next (roleActions.receiveRole(data));
            })  
            .catch (err => {
                next({
                    type: 'FETCH_ROLE_ERROR',err
                })
            });
            break;
            default:
                break;

    }
}
export default roleService;