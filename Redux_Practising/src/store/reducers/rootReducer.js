import  { combineReducers } from 'redux';
import counterReducer from '../reducers/conterReducer';
import newsReducer from '../reducers/newsReducer'
import authReducer from "./authreducer";
import roleReducer from './roleReducer';
import contacts from './contactReducer';


const rootReducer = combineReducers ({
    counterReducer,
    newsReducer,
    authReducer,
    roleReducer,
    contacts: contacts
});

export default rootReducer;