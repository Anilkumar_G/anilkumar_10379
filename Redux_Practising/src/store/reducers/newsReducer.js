import * as allActions from '../actions/actionConstants'

const intialState = {
    news : [],
    isLoaded : false,
    newsItem : {}
}

export default function newsReducer( state = intialState, action) {
    switch (action.type) {
        
        case allActions.FETCH_NEWS:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_NEWS:
            console.log("i am Receive")
            return {
                ...state,
                news: action.payload.articles,
                isLoaded: true
            };

        case allActions.FETCH_NEWS_BY_ID:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_NEWS_BY_ID:
            console.log("i am Receive")
            return {
                ...state,
                newsItem: action.payload.article,
                isLoaded: true
            };
        
            default:
            return state;
    }
}