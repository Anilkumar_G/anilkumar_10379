import * as  contactActions from '../actions/actionConstants';

export default (state = [], action) => {
    switch (action.type){
        case contactActions.CREATE_NEW_CONTACT:
            return [
                ...state,
                Object.assign({}, action.contact)
            ];
        
        case contactActions.DELETE_CONTACT:
            return state.filter((data, i) => i != action.id);
            
        default:
            return state;
    }
}