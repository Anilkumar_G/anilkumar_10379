import * as allActions from '../actions/actionConstants'

const intialState = {
    role  : '',
    isLoaded : false
    
}

export default function roleReducer( state = intialState, action) {
    switch (action.type) {
        
        case allActions.FETCH_ROLE:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_ROLE:
            console.log("i am Receive")
            return {
                ...state,
                role: action.payload.user.role,
                isLoaded: true
            };
            
            default:
            return state;
    }
}