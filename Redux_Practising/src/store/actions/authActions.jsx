import * as allActions from '../actions/actionConstants';

export function doLogInUser (data) {
    return { 
        type:allActions.DO_LOGIN_USER,
        payload : data 
    };
}

export function logInUserSuccess (data) {
    return {
        type : allActions.LOGIN_USER_SUCCESS,
        payload : data
    };
}

export function logOutUser () {
    return {
        type : allActions.LOGOUT_USER,
        payload : {}
    };
}