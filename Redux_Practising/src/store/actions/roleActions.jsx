import * as allActions from './actionConstants';

export function receiveRole(data) {
    return {
        type: allActions.RECEIVE_ROLE, payload : data
    };
}

export function fetchRole() {
    return {
        type: allActions.FETCH_ROLE, payload : {}
    };
}