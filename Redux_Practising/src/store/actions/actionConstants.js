export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
/**
 * 
 * Action Constants for news scenarios
 */
export const FETCH_NEWS = "FETCH_NEWS";
export const RECEIVE_NEWS = "RECEIVE_NEWS";

/**
 * Action constants for fetch news by Specific Id
 */

export const FETCH_NEWS_BY_ID = "FETCH_NEWS_BY_ID";
export const RECEIVE_NEWS_BY_ID = "RECEIVE_NEWS_BY_ID";

/**
 * Action constants for Login scenario
 */

export const DO_LOGIN_USER = "DO_LOGIN_USER";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_DATA_ERROR = 'LOGIN_USER_DATA_ERROR';

/**
 * Action Constants for Fetching User Details.
 */

export const FETCH_ROLE = "FETCH_ROLE";
export const RECEIVE_ROLE = "RECEIVE_ROLE";

/**
 * Action constants for Login scenario
 */

export const LOGOUT_USER  = "LOGOUT_USER";

/**
 * Action constants for Contact Application 
 */

 export const GET_ALL_CONTACTS = "GET_ALL_CONTACTS";
 export const CREATE_NEW_CONTACT = "CREATE_NEW_CONTACT";
 export const DELETE_CONTACT = "DELETE_CONTACT";