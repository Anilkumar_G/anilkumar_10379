import * as contactActions from './actionConstants';

export const createContact = (contact) => {
    debugger;
    return {
        type:contactActions.CREATE_NEW_CONTACT,
        contact : contact
    }
};

export const deleteContact = (id) => {
    debugger;
    return {
        type:contactActions.DELETE_CONTACT,
        id : id
    }
};