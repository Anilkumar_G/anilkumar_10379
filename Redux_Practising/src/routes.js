import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import {Switch,Route} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Home from './components/Home';
import Cars from './components/cars';
// import News from './components/news';
import CarDetails from './components/carDetails';
 import User from './components/userDashboard';
import newsDetails from './components/newsDetails';
import Login from './components/login';
import AdminDashBoard from './components/adminDashBoard';
import Redirect from './components/redirecter.';
import Recruiter from './components/RecruiterDashboard';
import AFC from './components/afcHeader';


const Routes = () => (
    <BrowserRouter>
    <Switch>
        <div>
        {/* <div>
            <Home />
        </div> */}
            <Route path = "/" component = {Home} exact />
            <Route path = "/clock" component = {Clock} exact/>
            <Route path = "/register" component = {Register} exact/>
            <Route path = "/cars" component = {Cars} exact/>
            <Route path = {"/cars/:id"} component = {CarDetails} exact />
            <Route path = "/news" component = {AFC} exact />
            <Route path = {"/news/:id"} component = {newsDetails} exact />
            <Route path = "/login" component = {Login} exact />
            <Route path = "/redirect" component = {Redirect} exact />
            <Route path = "/admin" component = {AdminDashBoard} exact />
            <Route path = "/recruiter" component = {Recruiter} exact />
            <Route path = "/user" component = {User} exact />
        </div>

    </Switch>
    </BrowserRouter>
);

export default Routes;