import React, { Component } from 'react';
import './afc.css';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';


class AFCHeader extends Component {
    constructor(props){
        super(props);

    }
    componentDidMount (){    
        this.props.newsActions.fetchNews();
        console.log (this.props.newsItems)
    }

    render() { 
        return ( 
            <div>
                <nav className="navbar navbar-expand" id="headerStyle">
                    <div className="container-fluid" >
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#" id="logo">AidForCancer</a>
                        </div>
                        <ul className="navbar-nav navbar-right"  >
                            <li className="nav-item"><a className="nav-link" href="#">Information</a></li>
                            <li className="nav-item" ><a className="nav-link" href="#"> Opinion</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Forum</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Volunteer</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Seek Help</a></li>
                            <li className="nav-item"><button className="btn" id="signButton">Sign In</button></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <div id="info">
                        <h3>Information About Cancer</h3>
                        <p>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                    </div>
                    <div className="row">
                        <div className="col-3">
                            <div className="vertical-menu">
                                <a href="#" class="active">News</a>
                                <a href="#">NGOs/NPOs</a>
                                <a href="#">Support Group</a>
                                <a href="#">Online Help</a>
                            </div>
                        </div>
                        <div className="col-9">
                            <h3 className="news">News</h3>
                            <a href="#" className="showall">Show all</a>
                            <br /><hr />
                            <div >
                                {
                                    (this.props.newsItems) ?
                                        <div>
                                            {this.props.newsItems.map((news =>
                                                (<div class="newsPosts" key={news._id}>
                                                    <img src="https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg" className="newsImg" />
                                                    <div className="newsLine">
                                                        <a href="news1.html" className="newsHead">{news.heading}</a>
                                                        <p>{news.brief}</p>
                                                    </div>
                                                </div>
                                                )
                                            ))};
                                        </div>
                                        : <div>
                                            Loading....
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                    </div>  
                    <div className="footer">
                        <p className="contact">
                            <a href="#" className="footlink" >Contact Us</a>
                        </p>
                        <button className="abusebtn">Report Abuse</button>
                        <p className="About">
                            <a href="#" className="footlink">About Us</a>
                        </p>
                        <p className="privacy">
                            <a href="#" className="footlink">  Privacy policy</a>
                        </p>
                        <p className="privacy">
                            <a href="" className="footlink"> Disclaimer</a>
                        </p>
                    </div>
                    <div className="copyright" >
                        <p style={{ marginTop: "5px" }}> <b> © 2018-19 Aid For Cancer</b></p>
                    </div>
                                   
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news,
    };

}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AFCHeader);