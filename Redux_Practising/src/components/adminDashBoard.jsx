import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import * as authActions from '../store/actions/authActions';
import ComponentHeader from './componentHeader';

class AdminDashBoard extends Component {

    componentDidMount() {
        this.props.authActions.logOutUser();
    }
    render() { 

        return ( 
            <div>
                <ComponentHeader />
                Welcome Admin
                <button onClick = {this.authActions.logOutUser}>LOGOUT</button>
            </div>

         );
    }
}

export default AdminDashBoard;