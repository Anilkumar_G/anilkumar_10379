import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Home extends Component {
    render() { 
        return (
             
            <div>
                <nav className="navbar navbar-expand" id="headerStyle">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#" id="logo">React World</a>
                        </div>
                            <ul className="navbar-nav navbar-right"  >
                            <Link to = "/clock"><li className="nav-item"><a className="nav-link" href="#">Clock</a></li></Link>
                            <Link to = "/register"><li className="nav-item" ><a className="nav-link" href="#"> Register</a></li></Link>
                            <Link to = "/cars"><li className="nav-item"><a className="nav-link" href="#"> Cars</a></li></Link>
                            <Link to = "/news"><li className="nav-item"><a className="nav-link" href="#">News</a></li></Link>
                            <li className="nav-item"><a className="nav-link" href="#"> Seek Help</a></li>
                            <Link to ="/login"><li className="nav-item"><button className="btn" id="signButton">Log In</button></li></Link>
                        </ul>
                    </div>
                </nav>
            </div>
         );
    }
}
 
export default Home;





// import React, { Component } from 'react';
// import {Link} from 'react-router-dom'

// class Home extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {  }
//     } 
//     render() { 
//         return ( 
//             <div>
//                 <h1>Welcome to my React World</h1>
//                 <Link to ="/login">Login</Link><br/>
//                 <Link to = "/clock">Clock</Link><br/>
//                 <Link to = "/register">Register</Link><br/>
//                 <Link to = "/cars">Wanna Buy a Car</Link><br/>
//                 <Link to = "/news">News</Link>

//             </div>
//          );
//     }
// }
 
// export default Home;
