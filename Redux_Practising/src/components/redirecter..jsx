import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import * as roleActions from '../store/actions/roleActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Redirecter extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    componentDidMount () {    
        this.props.roleActions.fetchRole() ;
    }
    render() {
        switch(this.props.user){
            case 'user':
            return <Redirect to="/user"></Redirect>
            case 'recruiter':
            return <Redirect to ="/recruiter"></Redirect>
            case 'admin':
            return <Redirect to = '/admin'/>
            default :
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.roleReducer.role,
    };

}

function mapDispatchToProps(dispatch) {
    return {
        roleActions: bindActionCreators(roleActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Redirecter);