import React, { Component } from 'react';
import './login.css';

class Footer extends Component {
    render() { 
        return ( 
            <div>
                <div className="footer">
                    <p className="contact">
                        <a href="#" className="footlink" >Contact Us</a>
                    </p>
                    <button className="abusebtn">Report Abuse</button>
                    <p className="About">
                        <a href="#" className="footlink">About Us</a>
                    </p>
                    <p className="privacy">
                        <a href="#" className="footlink">  Privacy policy</a>
                    </p>
                    <p className="privacy">
                        <a href="" className="footlink"> Disclaimer</a>
                    </p>
                </div>
                <div className="copyright" >
                    <p style={{ marginTop: "5px" }}> <b> © 2018-19 Aid For Cancer</b></p>
                </div>
            </div>
         );
    }
}
 
export default Footer ;