import React, { Component } from 'react';
import './login.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authActions from '../store/actions/authActions';
import { Redirect } from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false,

        }

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }

    doLogin = e => {
        this.props.authActions.doLogInUser({
            email: this.state.email,
            password: this.state.password
        });
        console.log(this.state.email)
        this.setState({
            isSubmitted: true
        });
    }

    render() {

        if (this.state.isSubmitted) {
            if (this.props.isLoggedIn === true) {
                window.location.reload();   
                return <Redirect to="/redirect" />
            }
            else {
                if (this.props.statusCode == -1) {
                    alert("Email id is not registered with us")
                    this.setState({ isSubmitted: false })
                }
                else if (this.props.statusCode == -2) {
                    alert("Incorrect password format")
                    this.setState({ isSubmitted: false })
                }
                else if (this.props.statusCode == -3) {
                    alert("Incorrect password")
                    this.setState({ isSubmitted: false })
                }
            }
        }

        return (
            <div className="body">
                <div>
                    <div className="header">
                        <div>Yo!<span>Aspire</span></div>
                    </div><br />
                    <div className="login">
                        {/* <form onSubmit = {this.doLogin} method = "post"> */}
                        <input type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Enter email"></input><br />
                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="password"></input>
                        <br />
                        <input type="submit" value="Login" onClick={this.doLogin} className="login-button"></input>
                        {/* </form> */}
                    </div>
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.authReducer.is_logged_in,
        statusCode: state.authReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);