import React, { Component } from 'react';
import './afc.css';
import {Link} from 'react-router-dom';
import {Redirect} from 'react-router-dom';

class componentHeader extends Component {
    constructor(props){
        super(props);
        this.state = {
            valid :false
        }
    }
   logOut = e => {
       localStorage.removeItem('jwt-token');
       window.location.reload();
       this.setState({valid :true})
   }

   render() {
  
       if(this.state.valid == true){
           return (
               <Redirect to = "/login" />
           )
       } 
        return ( 
            <div>
                <nav className="navbar navbar-expand" id="headerStyle">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#" id="logo">React World</a>
                        </div>
                            <ul className="navbar-nav navbar-right"  >
                            <Link to = "/clock"><li className="nav-item"><a className="nav-link" href="#">Clock</a></li></Link>
                            <Link to = "/register"><li className="nav-item" ><a className="nav-link" href="#"> Register</a></li></Link>
                            <Link to = "/cars"><li className="nav-item"><a className="nav-link" href="#"> Cars</a></li></Link>
                            <Link to = "/news"><li className="nav-item"><a className="nav-link" href="#">News</a></li></Link>
                            <li className="nav-item"><a className="nav-link" href="#"> Seek Help</a></li>
                            <li className="nav-item"><button className="btn" id="signButton" onClick = {this.logOut}>Log Out</button></li>
                        </ul>
                    </div>
                </nav>
            </div>
         );
    }
}
 
export default componentHeader;