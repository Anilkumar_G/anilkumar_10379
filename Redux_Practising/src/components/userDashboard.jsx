import React, { Component } from 'react';
import {Link, Redirect} from 'react-router-dom';
import * as authActions from '../store/actions/authActions';
import ComponentHeader from './componentHeader';


class UserDashBoard extends Component {
     constructor(props){
         super(props);
         this.state = {
             valid :false
         }
     }
    logOut = e => {
        localStorage.removeItem('jwt-token');
        this.setState({valid :true})
    }

    render() {
   
        if(this.state.valid == true){
            return (
                <Redirect to = "/login" />
            )
        } 
    
        return ( 
            <div>
                <ComponentHeader />
                Welcome User
                {/* <button onClick = {this.logOut}>LOGOUT</button> */}
            </div>

         );
    }
}
export default UserDashBoard;