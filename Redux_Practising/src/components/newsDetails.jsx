import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';
import './news.css';
import '../App.css'

class NewsDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedNews : {}
        }
    }
    componentWillMount (){
        
        this.props.newsActions.fetchNewsById(this.props.match.params.id);
    }
    componentWillReceiveProps(newProps) {
        if (newProps.newsItems) {
            this.setState({
                selectedNews: newProps.newsItems

            })
        }
    }

    render() {

        return (
            <div>
                <React.Fragment>0
                    <h2>Body: {this.state.selectedNews.body}</h2>
                </React.Fragment>
            </div>
        );

    }
}
function mapStateToProps(state) {
return {
    newsItems: state.newsReducer.newsItem,
};

}

function mapDispatchToProps(dispatch) {
return {
    newsActions: bindActionCreators(newsActions, dispatch),
};
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsDetails);