import React, { Component } from 'react';

class FetchAllUsers extends Component {
    constructor(props) {
        super(props);
        this.state = { 

            users : [],
            isLoaded :false

         }
    }

    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(data => { 
            this.setState ({
                isLoaded : true,
                users : data
                });
            })
        }
    render() {
        var { isLoaded, users } = this.state;
            if (!isLoaded){
                return <div>Loading....</div>
            } 
        return ( 
            <div className = "App">
                <ul>
                    {users.map (user =>
                        <li key ={user.id}>
                        Name  : {user.name} <br></br>
                        Email : {user.email}
                        </li>
                        )}

                </ul>
            </div>
         );
    }
}
 
export default FetchAllUsers;