import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';
import './news.css';
import {Link} from 'react-router-dom'
import '../App.css'

class News extends Component {
    constructor(props){
        super(props);

    }
    componentDidMount (){    
        this.props.newsActions.fetchNews();
      console.log (this.props.newsItems)
    }
    render (){
    return (
        <div>
            {
                (this.props.newsItems)?
                <div>
                    {this.props.newsItems.map((news =>
                        (<div key={news._id}>

                            <h2 className = "App"><a href = {`/news/${news._id}`} >{news.heading}</a></h2>
                            {/* <Link to = {'/news/' + news._id}>{news.heading}</Link> */}
                        </div>
                        )
                        ))};
                </div>
                :<div>
                    Loading....
                  </div>  
            }
        </div>
    );

}
}
function mapStateToProps(state) {
return {
    newsItems: state.newsReducer.news,
};

}

function mapDispatchToProps(dispatch) {
return {
    newsActions: bindActionCreators(newsActions, dispatch),
};
}

export default connect(mapStateToProps, mapDispatchToProps)(News);