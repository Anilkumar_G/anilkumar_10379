import React, { Component } from 'react';
import './App.css';
import Routes from './routes';
import AddContact from './contacts/AddContact';


class App extends Component {
  render() {

    return (
     
      <div className = "App"> 
        {/* <Routes /> */}
        <AddContact/>
      </div>
      
    
    );
  }
}

export default App;
