import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                <h1>Welcome to my World</h1>
                <Link to = "/clock">Clock</Link><br/>
                <Link to = "/register">Register</Link><br/>
                <Link to = "/cars">Wanna Buy a Car</Link>
            </div>
         );
    }
}
 
export default Home;
