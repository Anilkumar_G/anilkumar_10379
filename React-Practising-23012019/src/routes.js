import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import {Switch,Route} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Home from './components/Home';
import Cars from './components/cars'
import CarDetails from './components/carDetails'

const Routes = () => (
    <BrowserRouter>
    <Switch>
    <Route path = "/" component = {Home} exact/>
    <Route path = "/clock" component = {Clock} exact/>
    <Route path = "/register" component = {Register} exact/>
    <Route path = "/cars" component = {Cars} exact/>
    <Route path = {"/cars/:id"} component = {CarDetails} exact />
    </Switch>
    </BrowserRouter>
);

export default Routes;