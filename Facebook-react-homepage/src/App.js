import React, { Component } from 'react';
import Navbar from './components/Header/navbar';
import Leftbar from './components/leftbar/leftbar';
import Rightbar from './components/rightbar/ActiveFriends';
import Newsfeed from './components/newsfeed/newsfeed';
import Stories from './components/stories/stories';
import {Route} from 'react-router-dom';
import Friendlist from './components/Friendlists/friendlist';


class App extends Component {
  render() {
    
    return (
      <div>
        <Navbar/>
        <div class="container-fluid">
          <div class="row">
            <div class = "col-lg-2">
              <Leftbar/>
            </div> 
            <div class = "col-lg-6" >
             <Route path="/" exact component={Newsfeed}></Route>

              <Route path = "/friendslist"  component = {Friendlist}></Route>
            </div> 
            <div class = "col-lg-2">
            <Stories />
            </div>
            <div class = "col-lg-2" >
              <Rightbar/>
            </div>  
        </div>
        </div>
        <div>
             
        </div>  
      </div>
    );
  }
}

export default App;
