import React, { Component } from 'react';
import Router from './routes';
import Ne from './Components/newsArticle';

class App extends Component {
  render() {
    return (
      <div>
        <Router />
      </div>
    );
  }
}

export default App;
