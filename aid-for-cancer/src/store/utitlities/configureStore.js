import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import newsService from '../middlewares/newsService';

export default function configureStore() {
    return createStore (
        rootReducer,
        compose(applyMiddleware(
            newsService,
        ))
    );
};