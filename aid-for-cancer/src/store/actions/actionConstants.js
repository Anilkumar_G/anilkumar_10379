export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
/**
 * 
 * Action Constants for news scenarios
 */
export const FETCH_NEWS = "FETCH_NEWS";
export const RECEIVE_NEWS = "RECEIVE_NEWS";

/**
 * Action constants for fetch news by Specific Id
 */

export const FETCH_NEWS_BY_ID = "FETCH_NEWS_BY_ID";
export const RECEIVE_NEWS_BY_ID = "RECEIVE_NEWS_BY_ID";

/**
 * Action constants for Login scenario
 */

export const DO_LOGIN_USER = "DO_LOGIN_USER";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";


/**
 * Action constants for Logout scenario
 */

export const LOGOUT_USER  = "LOGOUT_USER"