import  { combineReducers } from 'redux';
import newsReducer from '../reducers/newsReducer'

const rootReducer = combineReducers ({
    newsReducer,

});

export default rootReducer;