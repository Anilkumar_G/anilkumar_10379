import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';
import './newsArticle.css'

class NewsArticle extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedNews : {}
        }
    }
    componentWillMount (){
        
        this.props.newsActions.fetchNewsById(this.props.match.params.id);
    }
    componentWillReceiveProps(newProps) {
        if (newProps.newsItems) {
            this.setState({
                selectedNews: newProps.newsItems

            })
        }
    }

    render() {

        return (
            <div>
               <div className = "news1">
        <p>
            <a href="home.html" className = "info">Information </a> 
            <a href="" className = "info">News</a>
        </p>
        <hr/><br/>
        <p className = "head">{this.state.selectedNews.heading}</p>
        <p className = "time">Posted on: {this.state.selectedNews.timestamp}</p><hr/>
        <br/>
        <div>
            <img src="news.jpg" className = "image"/>
            <h4 >{this.state.selectedNews.heading}</h4><br/>
            <p className = "brief"> {this.state.selectedNews.brief}</p>
        </div>
        <p className = "body">{this.state.selectedNews.body}</p>
        <br/><br/>
        <p>Related tags : <button class="btn1">{this.state.selectedNews.tags}</button></p>
        <br/><hr/><br/>
        <div className = "comments">
            <p className ="comments1">{this.state.selectedNews.comments} Comments :</p>
            <div style="padding:10px;background-color: rgb(171, 172, 177);margin-left: 10px;margin-right: 10px;">
                {/* <p>*Commented by <b> Rohit Mohanty</b> on 2018-12-12</p> */}
                <p>9th comment</p>
            </div><br/>
        </div>
    </div><br/>
    <div id="foot1" style="height:50px;background-color: rgb(228, 109, 115)" >
            <p style="float:left;margin-left: 220px;margin-top: 13px;">
                <a href="" style="text-decoration:none;color: white">Contact Us</a>
            </p>
            <button id="btn1" style="float:left;margin-left: 30px;margin-top: 10px;">Report Abuse</button>
            <p style="float:left;margin-left: 350px;margin-top: 13px;">
                <a href="" style="text-decoration:none;color: white">About Us</a> 
            </p>
            <p style="float:left;margin-left: 20px;margin-top: 13px;">
                <a href="" style="text-decoration:none;color: white">  Privacy policy</a>
            </p>
            <p style="float:left;margin-left: 20px;margin-top: 13px;">
                <a href="" style="text-decoration:none;color: white"> Disclaimer</a>
            </p> 
            </div>
            </div>

        );

    }
}

function mapStateToProps(state) {
return {
    newsItems: state.newsReducer.newsItem,
};

}

function mapDispatchToProps(dispatch) {
return {
    newsActions: bindActionCreators(newsActions, dispatch),
};
}

export default connect (mapStateToProps, mapDispatchToProps) (NewsArticle);