import React, { Component } from 'react';
import './afc.css';

class Header extends Component {
    render() { 
        return ( 
            <div>
                {/* <nav className="navbar navbar-expand" id="headerStyle">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#" id="logo">AidForCancer</a>
                        </div>
                        <ul className="navbar-nav navbar-right"  >
                            <li className="nav-item"><a className="nav-link" href="#">Information</a></li>
                            <li className="nav-item" ><a className="nav-link" href="#"> Opinion</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Forum</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Volunteer</a></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Seek Help</a></li>
                            <li className="nav-item"><button className="btn" id="signButton">Sign In</button></li>
                        </ul>
                    </div>
                </nav> */}


{/* <nav class="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#ee6e73" }}>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon">- </span>       
                </button>
                    <a class="navbar-brand" href="#" style={{ color: "white" }}>
                        <u style={{ fontFamily: "Montserrat,sans-serif", marginLeft: "20px" }}>AidforCancer</u>
                    </a>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav  navbar-right mr-auto" style={{marginLeft:"35vw"}}>
            <li className="nav-item  ">
              <a className="nav-link" href="#">Information  </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Opinions</a>
            </li>
            <li className="nav-item">
              <a className="nav-link " href="#">Forum</a>
            </li>
            <li className="nav-item">
              <a className="nav-link " href="#">Volunteer</a>
            </li>
            <li className="nav-item">
              <a className="nav-link " href="#">Seek Help</a>
            </li>
            <li className="nav-item">
             <buuton className="btn" id="signButton">SIGN-IN</buuton>
            </li>
          </ul>
        </div>
            </nav>  */}

<nav className="navbar navbar-expand-lg " id="headerStyle">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
      </button>
      <a className="navbar-brand" id ="logo" href="#">AidForCancer</a>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item" style={{ marginLeft: 550 }}>
            <a className="nav-link" href="#" >Information  </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">Opinions</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Forum</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Volunteer</a>
          </li>
          <li className="nav-item">
            <a className="nav-link " href="#">Seek Help</a>
          </li>
          <li className="nav-item">
            <button className="btn" id="signButton">Sign In</button>
          </li>
        </ul>
      </div>
    </nav> 

 {/* <nav class="navbar navbar-inverse" id="headerStyle">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a className="navbar-brand" href="#" id="logo">AidForCancer</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Page 1-1</a></li>
            <li><a href="#">Page 1-2</a></li>
            <li><a href="#">Page 1-3</a></li>
          </ul>
        </li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>  */}
            </div>
         );
    }
}
 
export default Header;