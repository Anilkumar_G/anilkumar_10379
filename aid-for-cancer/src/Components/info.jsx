import React, { Component } from 'react';
import './afc.css';

class Info extends Component {
    render() { 
        return ( 
            <div>
                <div id="info">
                    <h3>Information About Cancer</h3>
                    <p>Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                </div>
            </div>
         );
    }
}
 
export default Info;