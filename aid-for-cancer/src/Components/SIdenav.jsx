import React, { Component } from 'react';
import './afc.css';

class SideNav extends Component {
    render() { 
        return (
            <div> 
                <div>
                    <div>
                        <div id="vertical-menu">
                            <a href="#" class="active">News</a>
                            <a href="#">NGOs/NPOs</a>
                            <a href="#">Support Group</a>
                            <a href="#">Online Help</a>
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default SideNav ;