import React, { Component } from 'react';
import './afc.css';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as newsActions from '../store/actions/newsAcxtions';
import {Link} from 'react-router-dom';

class Article extends Component {
    constructor(props){
        super(props);

    }
    componentDidMount (){    
        this.props.newsActions.fetchNews();
        console.log (this.props.newsItems)
    }
    render() { 
        return ( 
    
            <div>
                <h3 className="news">News</h3>
                <a href="#" className="showall">Show all</a>
                <br /><hr />
                <div >
                    {
                        (this.props.newsItems) ?
                            <div>
                                {this.props.newsItems.map((news =>
                                    (<div class="newsPosts" key={news._id}>
                                        <img class="newsLogo" src={ "http://13.229.176.226:8001/"+news.image} alt="" className = "newsImg"/>
                                        <div>
                                            <p className="comment"><i className="fa fa-comments" >&nbsp;{news.comments}</i></p>
                                            <p className="like"><i className="fa fa-thumbs-up">&nbsp;{news.likes.likes}</i></p>
                                        </div>
                                        <div className="newsLine">
                                            <Link to={`/${news._id}`} className="newsHead">{news.heading}</Link>
                                            <p>{news.brief}</p>
                                        </div>
                                    </div>
                                    )
                                ))};
                            </div>
                            : <div>
                                Loading....
                            </div>
                    }
                </div>
                </div>
         );
    }
}
 
function mapStateToProps(state) {
    return {
        newsItems: state.newsReducer.news,
    };

}

function mapDispatchToProps(dispatch) {
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);
