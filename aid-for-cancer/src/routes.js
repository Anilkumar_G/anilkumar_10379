import React from 'react';
import {Switch,Route} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Header from './Components/HeaderAfc';
import SideNav from './Components/SIdenav';
import Footer from './Components/afcFooter';
import Articles from './Components/articles';
import Info from './Components/info';
import NewsArticle from './Components/newsArticle';

const Routes = () => (
    <BrowserRouter>
    <Switch>
            <div>
                <div>
                    <Header/>
                    <Route path="/:id" componet={NewsArticle} exact />
                </div>
                <div className="container">
                    <Info/>
                    <div className="row">
                        <div className ="col-md-3 col-sm-12" >
                            <SideNav />
                        </div>
                        <div className = "col-md-9 col-sm-12">
                            <Articles/>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
    </Switch>
    </BrowserRouter>
);

export default Routes;