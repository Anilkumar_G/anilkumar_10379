package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudentsServlet")
public class DisplayAllStudentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   		
   		HttpSession session = request.getSession();
   		if (session != null) {
   		StudentService service = new StudentServiceImpl();
   		List<Student> studentList = service.fetchAllStudents();
   		PrintWriter pw = response.getWriter();
   		if (studentList.size() != 0) {
   			JsonConverter json = new JsonConverter();
   			String result = json.convertToJson(studentList);
   			
   			pw.println(result);
   	   		pw.close();
   		}
   		else {
   			RequestDispatcher rd = request.getRequestDispatcher("error.htnl");
   	   		rd.forward(request, response);
   		}	
   		}else {
   			response.sendRedirect("Unauthorized.html");
   		}
   	}

}
