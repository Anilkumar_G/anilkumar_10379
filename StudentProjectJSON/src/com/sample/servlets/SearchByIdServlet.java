package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchByIdServlet")
public class SearchByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		if (session != null){
		int id = Integer.parseInt(request.getParameter("searchid"));
		StudentService service = new StudentServiceImpl();
		Student student = service.findById(id);

		RequestDispatcher rd = request.getRequestDispatcher("SearchById.jsp");
   		if(student != null) {
   			request.setAttribute("student", student );
   			rd.forward(request, response);
   		}else {
   			
   			RequestDispatcher rd1 = request.getRequestDispatcher("error.jsp");
   			rd1.forward(request, response);

   		}
		}else {
			response.sendRedirect("Unauthorized.html");
		}

	}

}
