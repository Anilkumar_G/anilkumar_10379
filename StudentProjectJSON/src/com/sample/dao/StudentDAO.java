package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;
/**
 * This the StudentDAO implementation class where all Abstract methods are included.
 * @author Anilkumar
 *
 */
public interface StudentDAO {
	
	/**
	 * This method calls to insert a new student details
	 * @param student
	 * @return
	 */
	boolean createStudent( Student student );
	/**
	 * This method calls to SEARCH  a  student details by student id..
	 * @param id
	 * @return
	 */
	Student searchById(int id);
	/**
	 * This method calls to Fetch All student details
	 * @return
	 */
	List<Student> getAllStudents();
	/**
	 * This method calls to update  a  student details.
	 * @param id
	 * @param age
	 * @param name
	 * @return
	 */
	public boolean UpdateById(int id , int age, String name);
	/**
	 * This method is used to delete an employee by id.
	 * @param id
	 * @return
	 */
	public boolean DeleteById(int id);
	
}
