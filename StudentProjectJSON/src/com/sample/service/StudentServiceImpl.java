package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

/**
 * The StudentServiceImpl class is used to implement all abstract methods
 * @author IMVIZAG
 *
 */
public class StudentServiceImpl implements StudentService{
	/**
	 * This is used to insert new student details into the database.
	 */
	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		
		return result;
	}
	/**
	 * This method gets the student details by using student_id
	 */
	@Override
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}
	/**
	 * This Method gets all student from database and shows to user
	 * @return student list
	 */
	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}
	/**
	 * This method update student based on given id
	 * @param id
	 * @param age
	 * @param name
	 * @return true/false
	 */
	public boolean UpdateById(int id , int age, String name) {
		
		StudentDAOImpl studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.UpdateById(id,age,name);
		return result;
	}
	/**
	 * This Method delete student from database based on given id
	 * @param id
	 * @return true / false
	 */
	public boolean DeleteById(int id) {
		
		StudentDAOImpl studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.DeleteById(id);
		return result;
	}

}














