package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
/**
 * The StudentService interface is used to declare the all abstract methods.
 * @author Anilkumar
 *
 */
public interface StudentService {
	/**
	 * This is used to insert new student details into the database.
	 * @param student
	 * @return
	 */
	boolean insertStudent( Student student );
	/**
	 * This method gets the student details by using student_id
	 * @param id
	 * @return
	 */
	Student findById(int id);
	/**
	 * This Method gets all student from database and shows to user
	 * @return student list
	 */
	List<Student> fetchAllStudents();
	/**
	 * This method update student based on given id
	 * @param id
	 * @param age
	 * @param name
	 * @return true/false
	 */
	public boolean UpdateById(int id , int age, String name);
	/**
	 * This Method delete student from database based on given id
	 * @param id
	 * @return true / false
	 */
	public boolean DeleteById(int id);
}
